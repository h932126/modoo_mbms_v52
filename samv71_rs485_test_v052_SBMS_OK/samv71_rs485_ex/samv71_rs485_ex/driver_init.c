/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <hal_init.h>
#include <hpl_pmc.h>
#include <peripheral_clk_config.h>
#include <utils.h>

/*! The buffer size for USART */
#define USART_1_BUFFER_SIZE 256
#define USART_2_BUFFER_SIZE 256
#define USART_3_BUFFER_SIZE 256
#define USART_4_BUFFER_SIZE 256

struct usart_async_descriptor USART_1;
struct usart_async_descriptor USART_2;
struct usart_async_descriptor USART_3;
struct usart_async_descriptor USART_4;

static uint8_t USART_1_buffer[USART_1_BUFFER_SIZE];
static uint8_t USART_2_buffer[USART_2_BUFFER_SIZE];
static uint8_t USART_3_buffer[USART_3_BUFFER_SIZE];
static uint8_t USART_4_buffer[USART_4_BUFFER_SIZE];

struct usart_sync_descriptor TARGET_IO;

void TARGET_IO_PORT_init(void)
{

	gpio_set_pin_function(PA9, MUX_PA9A_UART0_URXD0);

	gpio_set_pin_function(PA10, MUX_PA10A_UART0_UTXD0);
}

void TARGET_IO_CLOCK_init(void)
{
	_pmc_enable_periph_clock(ID_UART0);
}

void TARGET_IO_init(void)
{
	TARGET_IO_CLOCK_init();
	usart_sync_init(&TARGET_IO, UART0, _uart_get_usart_sync());
	TARGET_IO_PORT_init();
}

/**
 * \brief USART Clock initialization function
 *
 * Enables register interface and peripheral clock
 */
void USART_1_CLOCK_init()
{
	_pmc_enable_periph_clock(ID_UART1);
}

void USART_2_CLOCK_init()
{
	_pmc_enable_periph_clock(ID_UART2);
}

void USART_3_CLOCK_init()
{
	_pmc_enable_periph_clock(ID_UART3);
}

void USART_4_CLOCK_init()
{
	_pmc_enable_periph_clock(ID_UART4);
}


/**
 * \brief USART pinmux initialization function
 *
 * Set each required pin to USART functionality
 */
void USART_1_PORT_init()
{
	gpio_set_pin_function(PA5, MUX_PA5C_UART1_URXD1);
	gpio_set_pin_function(PA4, MUX_PA4C_UART1_UTXD1);
}

void USART_2_PORT_init()
{
	gpio_set_pin_function(PD25,  MUX_PD25C_UART2_URXD2);
	gpio_set_pin_function(PD26, MUX_PD26C_UART2_UTXD2);
}


void USART_3_PORT_init()
{
	gpio_set_pin_function(PD28,  MUX_PD28A_UART3_URXD3);
	gpio_set_pin_function(PD30, MUX_PD30A_UART3_UTXD3);
}

void USART_4_PORT_init()
{
	gpio_set_pin_function(PD18,  MUX_PD18C_UART4_URXD4);
	gpio_set_pin_function(PD19, MUX_PD19C_UART4_UTXD4);
}


/**
 * \brief USART initialization function
 *
 * Enables USART peripheral, clocks and initializes USART driver
 */
void USART_1_init(void)
{
	USART_1_CLOCK_init();
	usart_async_init(&USART_1, UART1, USART_1_buffer, USART_1_BUFFER_SIZE, _uart_get_usart_async());
	USART_1_PORT_init();
}

void USART_2_init(void)
{
	// LMS : 115200bps
	USART_2_CLOCK_init();
	usart_async_init(&USART_2, UART2, USART_2_buffer, USART_2_BUFFER_SIZE, _uart_get_usart_async());
	USART_2_PORT_init();
}

void USART_3_init(void)
{
	// ATS, UPS : 115200bps
	USART_3_CLOCK_init();
	usart_async_init(&USART_3, UART3, USART_3_buffer, USART_3_BUFFER_SIZE, _uart_get_usart_async());
	USART_3_PORT_init();
}

void USART_4_init(void)
{
	// Sub-MBS : 9600bps
	USART_4_CLOCK_init();
	usart_async_init(&USART_4, UART4, USART_4_buffer, USART_4_BUFFER_SIZE, _uart_get_usart_async());
	USART_4_PORT_init();
}

void system_init(void)
{
	init_mcu();

	_pmc_enable_periph_clock(ID_PIOA);

	// add by j : 20.50.15
	//_pmc_enable_periph_clock(ID_PIOC);

	// add by j : 20.50.04
	_pmc_enable_periph_clock(ID_PIOD);

	/* Disable Watchdog */
	hri_wdt_set_MR_WDDIS_bit(WDT);

	/* GPIO on PA3 */
	gpio_set_pin_level(RS485_TX1_EN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(RS485_TX1_EN, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(RS485_TX1_EN, GPIO_PIN_FUNCTION_OFF);


	/* GPIO on PD31 */
	gpio_set_pin_level(RS485_TX3_EN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(RS485_TX3_EN, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(RS485_TX3_EN, GPIO_PIN_FUNCTION_OFF);


	/* GPIO on PD24 */
	gpio_set_pin_level(RS485_TX4_EN,
	                   // <y> Initial level
	                   // <id> pad_initial_level
	                   // <false"> Low
	                   // <true"> High
	                   false);

	// Set pin direction to output
	gpio_set_pin_direction(RS485_TX4_EN, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(RS485_TX4_EN, GPIO_PIN_FUNCTION_OFF);

	

	// UART0 - Debug UART
	TARGET_IO_init();

	// "USART_0_init""에서 USART_1_init"로 변경. by J
	USART_1_init();

	USART_2_init();

	USART_3_init();

	USART_4_init();

	
}
