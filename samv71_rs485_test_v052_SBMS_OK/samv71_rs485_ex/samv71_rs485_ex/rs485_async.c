#include "driver_examples.h"
#include "driver_init.h"
#include "rs485_async.h"
#include "Update_List.h"

#include <string.h>

static struct io_descriptor *rs485_io_01 = NULL;
static struct io_descriptor *rs232_io_02 = NULL;
static struct io_descriptor *rs485_io_03 = NULL;
static struct io_descriptor *rs485_io_04 = NULL;

static const unsigned short wCRCTable[] = {
    0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241, 0xC601, 0x06C0,
    0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440, 0xCC01, 0x0CC0, 0x0D80, 0xCD41,
    0x0F00, 0xCFC1, 0xCE81, 0x0E40, 0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0,
    0x0880, 0xC841, 0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
    0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41, 0x1400, 0xD4C1,
    0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641, 0xD201, 0x12C0, 0x1380, 0xD341,
    0x1100, 0xD1C1, 0xD081, 0x1040, 0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1,
    0xF281, 0x3240, 0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
    0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41, 0xFA01, 0x3AC0,
    0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840, 0x2800, 0xE8C1, 0xE981, 0x2940,
    0xEB01, 0x2BC0, 0x2A80, 0xEA41, 0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1,
    0xEC81, 0x2C40, 0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
    0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041, 0xA001, 0x60C0,
    0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240, 0x6600, 0xA6C1, 0xA781, 0x6740,
    0xA501, 0x65C0, 0x6480, 0xA441, 0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0,
    0x6E80, 0xAE41, 0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
    0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41, 0xBE01, 0x7EC0,
    0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40, 0xB401, 0x74C0, 0x7580, 0xB541,
    0x7700, 0xB7C1, 0xB681, 0x7640, 0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0,
    0x7080, 0xB041, 0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
    0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440, 0x9C01, 0x5CC0,
    0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40, 0x5A00, 0x9AC1, 0x9B81, 0x5B40,
    0x9901, 0x59C0, 0x5880, 0x9841, 0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1,
    0x8A81, 0x4A40, 0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
    0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641, 0x8201, 0x42C0,
    0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040};

static const unsigned short crc15_Table[] = { 
	  0x0000,0xc599,0xceab,0x0b32,0xd8cf,0x1d56,0x1664,0xd3fd,
	  0xf407,0x319e,0x3aac,0xff35,0x2cc8,0xe951,0xe263,0x27fa,
	  0xad97,0x680e,0x633c,0xa6a5,0x7558,0xb0c1,0xbbf3,0x7e6a,
	  0x5990,0x9c09,0x973b,0x52a2,0x815f,0x44c6,0x4ff4,0x8a6d,
	  0x5b2e,0x9eb7,0x9585,0x501c,0x83e1,0x4678,0x4d4a,0x88d3,
	  0xaf29,0x6ab0,0x6182,0xa41b,0x77e6,0xb27f,0xb94d,0x7cd4,
	  0xf6b9,0x3320,0x3812,0xfd8b,0x2e76,0xebef,0xe0dd,0x2544,
	  0x02be,0xc727,0xcc15,0x098c,0xda71,0x1fe8,0x14da,0xd143,
	  0xf3c5,0x365c,0x3d6e,0xf8f7,0x2b0a,0xee93,0xe5a1,0x2038,
	  0x07c2,0xc25b,0xc969,0x0cf0,0xdf0d,0x1a94,0x11a6,0xd43f,
	  0x5e52,0x9bcb,0x90f9,0x5560,0x869d,0x4304,0x4836,0x8daf,
	  0xaa55,0x6fcc,0x64fe,0xa167,0x729a,0xb703,0xbc31,0x79a8,
	  0xa8eb,0x6d72,0x6640,0xa3d9,0x7024,0xb5bd,0xbe8f,0x7b16,
	  0x5cec,0x9975,0x9247,0x57de,0x8423,0x41ba,0x4a88,0x8f11,
	  0x057c,0xc0e5,0xcbd7,0x0e4e,0xddb3,0x182a,0x1318,0xd681,
	  0xf17b,0x34e2,0x3fd0,0xfa49,0x29b4,0xec2d,0xe71f,0x2286,
	  0xa213,0x678a,0x6cb8,0xa921,0x7adc,0xbf45,0xb477,0x71ee,
	  0x5614,0x938d,0x98bf,0x5d26,0x8edb,0x4b42,0x4070,0x85e9,
	  0x0f84,0xca1d,0xc12f,0x04b6,0xd74b,0x12d2,0x19e0,0xdc79,
	  0xfb83,0x3e1a,0x3528,0xf0b1,0x234c,0xe6d5,0xede7,0x287e,
	  0xf93d,0x3ca4,0x3796,0xf20f,0x21f2,0xe46b,0xef59,0x2ac0,
	  0x0d3a,0xc8a3,0xc391,0x0608,0xd5f5,0x106c,0x1b5e,0xdec7,
	  0x54aa,0x9133,0x9a01,0x5f98,0x8c65,0x49fc,0x42ce,0x8757,
	  0xa0ad,0x6534,0x6e06,0xab9f,0x7862,0xbdfb,0xb6c9,0x7350,
	  0x51d6,0x944f,0x9f7d,0x5ae4,0x8919,0x4c80,0x47b2,0x822b,
	  0xa5d1,0x6048,0x6b7a,0xaee3,0x7d1e,0xb887,0xb3b5,0x762c,
	  0xfc41,0x39d8,0x32ea,0xf773,0x248e,0xe117,0xea25,0x2fbc,
	  0x0846,0xcddf,0xc6ed,0x0374,0xd089,0x1510,0x1e22,0xdbbb,
	  0x0af8,0xcf61,0xc453,0x01ca,0xd237,0x17ae,0x1c9c,0xd905,
	  0xfeff,0x3b66,0x3054,0xf5cd,0x2630,0xe3a9,0xe89b,0x2d02,
	  0xa76f,0x62f6,0x69c4,0xac5d,0x7fa0,0xba39,0xb10b,0x7492,
	  0x5368,0x96f1,0x9dc3,0x585a,0x8ba7,0x4e3e,0x450c,0x8095};

#define STX '{' 
#define ETX '}'
#define CR  0x0D
#define LF  0x0A

#define RX_DATA_MAX_SIZE 512
#define TX_DATA_MAX_SIZE 32

static uint8_t usart_rx_01[RX_DATA_MAX_SIZE] = { 0x00, };	
static uint8_t usart_rx_02[RX_DATA_MAX_SIZE] = { 0x00, };	
static uint8_t usart_rx_03[RX_DATA_MAX_SIZE] = { 0x00, };	
static uint8_t usart_rx_04[RX_DATA_MAX_SIZE] = { 0x00, };	


static unsigned char usart_tx_buffer[TX_DATA_MAX_SIZE] = { 0x00, };	


static unsigned char usart_tx_01_buffer[TX_DATA_MAX_SIZE] = { 0x00, };	
static unsigned char usart_tx_02_buffer[TX_DATA_MAX_SIZE] = { 0x00, };	
static unsigned char usart_tx_03_buffer[TX_DATA_MAX_SIZE] = { 0x00, };	
static unsigned char usart_tx_04_buffer[TX_DATA_MAX_SIZE] = { 0x00, };	


//static unsigned char  usart_tx_04_cnt =0;


static bool flag_rx_01 = false;
static bool flag_rx_02 = false;
static bool flag_rx_03 = false;
static bool flag_rx_04 = false;

static unsigned short TXD_CRC={ 16 };		//initialize the PEC
static unsigned short RXD_CRC={ 16 };		//initialize the PEC


static void rs485_tx_cb_01(const struct usart_async_descriptor *const io_descr)
{
	/* Transfer completed */
	// 최대한 빠르게 "ADM2682E"의 DE(Device Enable Input)를 low로 설정해야 함.
	// "ADM2682E"의 DE(Device Enable Input)이 High일 경우 Rx 수신 안됨.
	// "delay_ms(5);" 코드 삭제
	//delay_ms(5);
	gpio_set_pin_level(RS485_TX1_EN, false);
}


static void rs232_tx_cb_02(const struct usart_async_descriptor *const io_descr)
{
	/* Transfer completed */	
}


static void rs485_tx_cb_03(const struct usart_async_descriptor *const io_descr)
{
	/* Transfer completed */
	// 최대한 빠르게 "ADM2682E"의 DE(Device Enable Input)를 low로 설정해야 함.
	// "ADM2682E"의 DE(Device Enable Input)이 High일 경우 Rx 수신 안됨.
	// "delay_ms(5);" 코드 삭제
	//delay_ms(5);
	gpio_set_pin_level(RS485_TX3_EN, false);
}

static void rs485_tx_cb_04(const struct usart_async_descriptor *const io_descr)
{
	/* Transfer completed */
	// 최대한 빠르게 "ADM2682E"의 DE(Device Enable Input)를 low로 설정해야 함.
	// "ADM2682E"의 DE(Device Enable Input)이 High일 경우 Rx 수신 안됨.
	// "delay_ms(5);" 코드 삭제
	//delay_ms(5);
	gpio_set_pin_level(RS485_TX4_EN, false);
}


static void rs485_rx_cb_01(const struct usart_async_descriptor *const io_descr)
{
	/* RX completed */
	flag_rx_01 = true;
}

static void rs232_rx_cb_02(const struct usart_async_descriptor *const io_descr)
{
	/* RX completed */
	flag_rx_02 = true;
}

static void rs485_rx_cb_03(const struct usart_async_descriptor *const io_descr)
{
	/* RX completed */
	flag_rx_03 = true;	
}

static void rs485_rx_cb_04(const struct usart_async_descriptor *const io_descr)
{
	/* RX completed */
	flag_rx_04 = true;	

}


static void rs485_err_cb_01(const struct usart_async_descriptor *const io_descr)
{
	/* Transfer completed */
	printf("rs485 #01-Err\r\n");
}

static void rs232_err_cb_02(const struct usart_async_descriptor *const io_descr)
{
	/* Transfer completed */
	printf("232-02-Err\r\n");
}


static void rs485_err_cb_03(const struct usart_async_descriptor *const io_descr)
{
	/* Transfer completed */
	printf("rs485 #03-Err\r\n");
}

static void rs485_err_cb_04(const struct usart_async_descriptor *const io_descr)
{
	/* Transfer completed */
	printf("rs485 #04-Err\r\n");
}


void rs232_init(void)
{
	//===================================
	// RS232 - UART1, LMS / 115200bps
	//===================================
	usart_async_register_callback(&USART_2, USART_ASYNC_TXC_CB, rs232_tx_cb_02);
	usart_async_register_callback(&USART_2, USART_ASYNC_RXC_CB, rs232_rx_cb_02);
	usart_async_register_callback(&USART_2, USART_ASYNC_ERROR_CB, rs232_err_cb_02);
	
	usart_async_get_io_descriptor(&USART_2, &rs232_io_02);
	usart_async_enable(&USART_2);

}


void rs485_init(void)
{

	//===================================
	// RS482 - UART1, VBOX / 115200bps
	//===================================
	usart_async_register_callback(&USART_1, USART_ASYNC_TXC_CB, rs485_tx_cb_01);
	usart_async_register_callback(&USART_1, USART_ASYNC_RXC_CB, rs485_rx_cb_01);
	usart_async_register_callback(&USART_1, USART_ASYNC_ERROR_CB, rs485_err_cb_01);
	
	usart_async_get_io_descriptor(&USART_1, &rs485_io_01);
	usart_async_enable(&USART_1);

	//===================================
	// RS482 - UART3, ATS & UPS Device / 115200bps
	//===================================	
	usart_async_register_callback(&USART_3, USART_ASYNC_TXC_CB, rs485_tx_cb_03);
	usart_async_register_callback(&USART_3, USART_ASYNC_RXC_CB, rs485_rx_cb_03);
	usart_async_register_callback(&USART_3, USART_ASYNC_ERROR_CB, rs485_err_cb_03);


	usart_async_get_io_descriptor(&USART_3, &rs485_io_03);
	usart_async_enable(&USART_3);

	//===================================
	// RS482 - UART4 : Sub-BMS / 9600bps
	//===================================	
	usart_async_register_callback(&USART_4, USART_ASYNC_TXC_CB, rs485_tx_cb_04);
	usart_async_register_callback(&USART_4, USART_ASYNC_RXC_CB, rs485_rx_cb_04);
	usart_async_register_callback(&USART_4, USART_ASYNC_ERROR_CB, rs485_err_cb_04);


	usart_async_get_io_descriptor(&USART_4, &rs485_io_04);
	usart_async_enable(&USART_4);


	
}

void rs485_tx( uint8_t *buff, uint16_t length )
{
	if ( rs485_io_01 != NULL )
	{
		gpio_set_pin_level(RS485_TX1_EN, true);
		delay_ms(5);
		io_write(rs485_io_01, buff, length);
		
		printf("\r\n");
		printf("[Debug : 485-01-TX, %.*s]\r\n", length, buff);
	}
}


//====================================================
// VBOX Device RS485 Tx
// - UART1(485)
// - 115200bps
// -  RTU 
//====================================================
void send_VBOX(uint8_t p_addr, uint8_t p_func, uint16_t p_s_addr, uint16_t p_len)
{

	uint8_t Tx_cnt = 0;
	unsigned short crc16=0;
       	    	
	memset(usart_tx_01_buffer, 0x00, TX_DATA_MAX_SIZE);	
	
	usart_tx_01_buffer[Tx_cnt++] = p_addr;
	usart_tx_01_buffer[Tx_cnt++] = p_func;
	usart_tx_01_buffer[Tx_cnt++] = (unsigned char)((p_s_addr>>8) & 0xff);
	usart_tx_01_buffer[Tx_cnt++] = (unsigned char)(p_s_addr & 0xff);

	// Function 1번은 장비 On/Off 제어 
	if(p_func == 1) 
	{
		// Funtcion "1" 이면 p_len가 1byte On/Off 제어 
		usart_tx_01_buffer[Tx_cnt++] = p_len & 0xff;
	}
	else
	{
		usart_tx_01_buffer[Tx_cnt++] = (unsigned char)((p_len>>8) & 0xff);
		usart_tx_01_buffer[Tx_cnt++] = (unsigned char)(p_len & 0xff);
	}

	crc16 = CRC16(usart_tx_01_buffer, Tx_cnt);


	//==========================================================================
	// 확인필요 
	// CRC 2byte가 Little Endian으로 처리해야 ATS Board 및 UPS Board와 통신 됨.	
	//==========================================================================
	usart_tx_01_buffer[Tx_cnt++] = (unsigned char)((crc16>>0) & 0x00ff);
	usart_tx_01_buffer[Tx_cnt++] = (unsigned char)((crc16>>8) & 0x00ff);

	/*
	//==========================================================================
	// Error 발생.	
	// CRC 2byte가 Big Endian으로 처리 시  ATS Board 및 UPS Board와 통신 안됨.
	//==========================================================================
	usart_tx_buffer[Tx_cnt++] = (unsigned char)((crc16>>8) & 0x00ff);
	usart_tx_buffer[Tx_cnt++] = (unsigned char)((crc16>>0) & 0x00ff);
	*/
	
	if(rs485_io_01!=NULL)
	{
		gpio_set_pin_level(RS485_TX1_EN, true);
		delay_ms(5);
		io_write(rs485_io_01, usart_tx_01_buffer, Tx_cnt);
	}
	else
	{
#if defined(DEBUG_UART_1_TX)
		printf("[ERROR] Check rs485_io_01");
#endif
	}
						

#if defined(DEBUG_UART_1_TX)

	printf("\r\n VBOX Tx  = ");

	for(int i=0; i<Tx_cnt; i++)
	{
		printf("%02x", usart_tx_01_buffer[i]);
	}

	printf("\r\n");

#endif 

	
}


//====================================================
// ATS Device & UPS Device RS485 Tx
// - UART3(485)
// - 115200bps
// -  RTU 
//====================================================
void send_ATS_UPS(uint8_t p_addr, uint8_t p_func, uint16_t p_s_addr, uint16_t p_len)
{

	uint8_t Tx_cnt = 0;
	unsigned short crc16=0;
       	    	
	memset(usart_tx_03_buffer, 0x00, TX_DATA_MAX_SIZE);	
	
	usart_tx_03_buffer[Tx_cnt++] = p_addr;
	usart_tx_03_buffer[Tx_cnt++] = p_func;
	usart_tx_03_buffer[Tx_cnt++] = (unsigned char)((p_s_addr>>8) & 0xff);
	usart_tx_03_buffer[Tx_cnt++] = (unsigned char)(p_s_addr & 0xff);

	// Function 1번은 장비 On/Off 제어 
	if(p_func == 1) 
	{
		// Funtcion "1" 이면 p_len가 1byte On/Off 제어 
		usart_tx_03_buffer[Tx_cnt++] = p_len & 0xff;
	}
	else
	{
		usart_tx_03_buffer[Tx_cnt++] = (unsigned char)((p_len>>8) & 0xff);
		usart_tx_03_buffer[Tx_cnt++] = (unsigned char)(p_len & 0xff);
	}

	crc16 = CRC16(usart_tx_03_buffer, Tx_cnt);


	//==========================================================================
	// 확인필요 
	// CRC 2byte가 Little Endian으로 처리해야 ATS Board 및 UPS Board와 통신 됨.	
	//==========================================================================
	usart_tx_03_buffer[Tx_cnt++] = (unsigned char)((crc16>>0) & 0x00ff);
	usart_tx_03_buffer[Tx_cnt++] = (unsigned char)((crc16>>8) & 0x00ff);

	/*
	//==========================================================================
	// Error 발생.	
	// CRC 2byte가 Big Endian으로 처리 시  ATS Board 및 UPS Board와 통신 안됨.
	//==========================================================================
	usart_tx_buffer[Tx_cnt++] = (unsigned char)((crc16>>8) & 0x00ff);
	usart_tx_buffer[Tx_cnt++] = (unsigned char)((crc16>>0) & 0x00ff);
	*/
	
	if(rs485_io_03!=NULL)
	{
		gpio_set_pin_level(RS485_TX3_EN, true);
		delay_ms(5);
		io_write(rs485_io_03, usart_tx_03_buffer, Tx_cnt);
	}
	else
	{
		printf("[ERROR] Check rs485_io_03");
	}
						

#if defined(DEBUG_UART_3_TX)

	printf("\r\n ATS & UPS Tx = ");

	for(int i=0; i<Tx_cnt; i++)
	{
		printf("%02x", usart_tx_03_buffer[i]);
	}

	printf("\r\n");

#endif 

	
}


void ByteToChar2(uint8_t dt, uint8_t *buff, uint8_t p_index , uint8_t p_c , uint8_t p_crc)
{
	unsigned char a;
	unsigned char dat;

	a= dt/16;
	if(a>9) dat= (a+'A')-10;
	else	dat= (a+'0');

	buff[p_index] = dat;	

	if(p_crc) 
	{
		CRC16_Tx_ASCII(dat);	
	}

	a= dt%16;

	if(a>9) dat= (a+'A')-10;
	else	dat= (a+'0');

	buff[p_index+1]  = dat;

	if(p_crc) 
	{
		CRC16_Tx_ASCII(dat);
	}

	if(p_c)
	{
		buff[p_index+2]  = p_c;
	}	

}

//============================================================
// RS485 Tx Func 
//   - Sub BMS
//  -  9600bps
//============================================================
void send_SBMS(uint8_t p_addr, uint8_t p_func, uint16_t p_cmd)
{
	uint8_t Tx_cnt = 0;
	
	memset(usart_tx_04_buffer, 0x00, TX_DATA_MAX_SIZE);

	TXD_CRC = 0x10;
		
	usart_tx_04_buffer[Tx_cnt++] = STX;

	// Addres
	ByteToChar2(p_addr, usart_tx_04_buffer, Tx_cnt, ',', 1);	
	Tx_cnt += 3;

	// Function
	ByteToChar2(p_func, usart_tx_04_buffer, Tx_cnt, ',', 1);	
	Tx_cnt += 3;

	// Command
	ByteToChar2(p_cmd, usart_tx_04_buffer, Tx_cnt, ',', 1);	
	Tx_cnt += 3;

	// Data 0
	ByteToChar2(0x00, usart_tx_04_buffer, Tx_cnt, ',', 1);	
	Tx_cnt += 3;

	// Data 1
	ByteToChar2(0x00, usart_tx_04_buffer, Tx_cnt, '*', 1);	
	Tx_cnt += 3;

	// Data CRC
	ByteToChar2(((TXD_CRC>>8)&0xff), usart_tx_04_buffer, Tx_cnt, 0, 0);	
	Tx_cnt += 2;

	ByteToChar2((TXD_CRC&0xff), usart_tx_04_buffer, Tx_cnt, 0, 0);	
	Tx_cnt += 2;

	usart_tx_04_buffer[Tx_cnt++] =ETX;

	if(rs485_io_04!=NULL)
	{
		gpio_set_pin_level(RS485_TX4_EN, true);
		delay_ms(5);
		io_write(rs485_io_04, usart_tx_04_buffer, Tx_cnt);
	}
	else
	{
#if defined(DEBUG_UART_4_TX)	
		printf("[ERROR] Check rs485_io_04");
#endif
	}

#if defined(DEBUG_UART_4_TX)

	printf("\r\n#4 SBMS  Tx : ");

	for(int i=0; i<Tx_cnt; i++)
	{
		printf("%02x,", usart_tx_04_buffer[i]);
	}

	printf("\r\n");

#endif 	

}

void rs232_tx_02( uint8_t *buff, uint16_t length )
{

	if ( rs232_io_02 != NULL )
	{
		io_write(rs232_io_02, buff, length);
		
		printf("\r\n");
		printf("[Debug : 232-02-TX, %.*s]\r\n", length, buff);
	}

	
}

void rs485_tx_03( uint8_t *buff, uint16_t length )
{
	if ( rs485_io_03 != NULL )
	{
		gpio_set_pin_level(RS485_TX3_EN, true);
		delay_ms(5);
		io_write(rs485_io_03, buff, length);
		
		printf("\r\n");
		printf("[Debug : 485-03-TX, %.*s]\r\n", length, buff);
	}
}

void rs485_tx_04( uint8_t *buff, uint16_t length )
{
	if ( rs485_io_04 != NULL )
	{
		gpio_set_pin_level(RS485_TX4_EN, true);
		delay_ms(5);
		io_write(rs485_io_04, buff, length);
		
		printf("\r\n");
		printf("[Debug : 485-04-TX, %.*s]\r\n", length, buff);
	}
}

unsigned short CRC16(const unsigned char *nData, unsigned char wLength)
{
    unsigned char nTemp;
    unsigned short wCRCWord = 0xFFFF;
    while (wLength--) {
        nTemp = *nData++ ^ wCRCWord; wCRCWord >>= 8;
        wCRCWord ^= wCRCTable[nTemp];
    }
    return wCRCWord;
}
void CRC16_Tx_ASCII(unsigned char dat)
{
	unsigned short addr;
	addr    = ((TXD_CRC>>7)^dat)&0xff;		//calculate PEC table address
	TXD_CRC = ( TXD_CRC<<8)^crc15_Table[addr];
}

void CRC16_Rx_ASCII(unsigned char dat)
{
	unsigned short addr;
	addr    = ((RXD_CRC>>7)^dat)&0xff;		//calculate PEC table address
	RXD_CRC = ( RXD_CRC<<8)^crc15_Table[addr];
}


void rs485_get_rx_VBOX( )
{
	if ( rs485_io_01 != NULL )
	{
		if(flag_rx_01)
		{	
			// Rx Buffer Reset
			memset(usart_rx_01,0x00, RX_DATA_MAX_SIZE);

			uint16_t read_bytes = io_read(rs485_io_01, usart_rx_01, RX_DATA_MAX_SIZE);			
			usart_rx_01[read_bytes] = '\0';

#if defined(DEBUG_UART_1_RX)
			
			printf("\r\n#VBOX Rx len =%d, ", read_bytes);

			for (int loop = 0; loop < read_bytes; loop++)
			{
				//printf("<%02x>", usart_rx_03[loop]);
				printf("%02x", usart_rx_01[loop]);
			}
			
#endif
			//====================================================
			// 간단히 CRC 체크만.
			//====================================================

			// 6byte이하로 수신되는 경우 Error
			if(read_bytes > 6)
			{
				// crc 2byte 제외
				unsigned short crc16=0;
				unsigned short crc16_read=0;
								
				crc16 = CRC16(usart_rx_01, read_bytes-2);

				//====================================================================
				// [주의]
				// ATS board와 UPS board 보드의 CRC가 Little Endian으로 와서 
				// Byte 순서를 바꾸었음. 
				// ATS board와 UPS board 보드의 CRC 처리 방식이 변경되면 꼭 수정해야 함.
				//====================================================================
				crc16_read = usart_rx_01[read_bytes-1]<<8;								
				crc16_read = crc16_read + usart_rx_01[read_bytes-2];				
				
				if(crc16 == crc16_read)
				{
#if defined(DEBUG_UART_3_RX)
					printf("\r\n#VBOX Rx CRC OK : %04x, %04x", crc16, crc16_read);
#endif					
				}
				else
				{
#if defined(DEBUG_UART_3_RX)
					printf("\r\n[ERROR]#VBOX Rx CRC Error : %04x, %04x", crc16, crc16_read);
#endif					
				}
				
			}
			else
			{
#if defined(DEBUG_UART_3_RX)			
				printf("\r\n[ERROR]#VBOX Rx len =%d, ", read_bytes);
#endif		
			}

			flag_rx_01 = 0;
			
		}
		
	}
}

// UART2(232)/115200bps/RTU : LMS
void rs232_get_rx_LMS( )
{

	if ( rs232_io_02 != NULL )
	{
		if(flag_rx_02)
		{	
			// Rx Buffer Reset
			memset(usart_rx_02,0x00, RX_DATA_MAX_SIZE);

			uint16_t read_bytes = io_read(rs232_io_02, usart_rx_02, RX_DATA_MAX_SIZE);			
			usart_rx_02[read_bytes] = '\0';

#if defined(DEBUG_UART_2_RX)
			
			printf("\r\n#LMS Rx len =%d, ", read_bytes);

			for (int loop = 0; loop < read_bytes; loop++)
			{
				//printf("<%02x>", usart_rx_03[loop]);
				printf("%02x", usart_rx_02[loop]);
			}
			
#endif

			flag_rx_02 = 0;
			
		}
		
	}

		
}

void rs485_get_rx_ATS_UPS( )
{
	if ( rs485_io_03 != NULL )
	{
		if(flag_rx_03)
		{	
			// Rx Buffer Reset
			memset(usart_rx_03,0x00, RX_DATA_MAX_SIZE);

			uint16_t read_bytes = io_read(rs485_io_03, usart_rx_03, RX_DATA_MAX_SIZE);			
			usart_rx_03[read_bytes] = '\0';

#if defined(DEBUG_UART_3_RX)
			
			printf("\r\n#ATS&UPS Rx len =%d, ", read_bytes);

			for (int loop = 0; loop < read_bytes; loop++)
			{
				//printf("<%02x>", usart_rx_03[loop]);
				printf("%02x", usart_rx_03[loop]);
			}
			
#endif
			//====================================================
			// 간단히 CRC 체크만.
			//====================================================

			// 6byte이하로 수신되는 경우 Error
			if(read_bytes > 6)
			{
				// crc 2byte 제외
				unsigned short crc16=0;
				unsigned short crc16_read=0;
								
				crc16 = CRC16(usart_rx_03, read_bytes-2);

				//====================================================================
				// [주의]
				// ATS board와 UPS board 보드의 CRC가 Little Endian으로 와서 
				// Byte 순서를 바꾸었음. 
				// ATS board와 UPS board 보드의 CRC 처리 방식이 변경되면 꼭 수정해야 함.
				//====================================================================
				crc16_read = usart_rx_03[read_bytes-1]<<8;								
				crc16_read = crc16_read + usart_rx_03[read_bytes-2];				
				
				if(crc16 == crc16_read)
				{
#if defined(DEBUG_UART_3_RX)
					printf("\r\n#ATS&UPS Rx CRC OK : %04x, %04x", crc16, crc16_read);
#endif					
				}
				else
				{
#if defined(DEBUG_UART_3_RX)
					printf("\r\n[ERROR]#ATS&UPS Rx CRC Error : %04x, %04x", crc16, crc16_read);
#endif					
				}
				
			}
			else
			{
#if defined(DEBUG_UART_3_RX)			
				printf("\r\n[ERROR]#ATS&UPS Rx len =%d, ", read_bytes);
#endif		
			}

			flag_rx_03 = 0;
			
		}
		
	}
}


void rs485_get_rx_SBMS( )
{
	if ( rs485_io_04 != NULL )
	{
		if(flag_rx_04)
		{	
			// Rx Buffer Reset
			memset(usart_rx_04,0x00, RX_DATA_MAX_SIZE);

			uint16_t read_bytes = io_read(rs485_io_04, usart_rx_04, RX_DATA_MAX_SIZE);			
			usart_rx_04[read_bytes] = '\0';
			
			printf("SBMS Rx len =%d, ", read_bytes);

			for (int loop = 0; loop < read_bytes; loop++)
			{
				//printf("<%02x>", usart_rx_04[loop]);
				printf("%02x,", usart_rx_04[loop]);
			}


			// CRC Check
			unsigned short crc=0;

			bool flag_crc = false;			
						
			RXD_CRC = 0x10;		//initialize the PEC

			for (int loop = 0; loop < read_bytes; loop++)
			{
				if(usart_rx_04[loop] != STX && usart_rx_04[loop] != ',' && usart_rx_04[loop] != '*' && usart_rx_04[loop] != ETX) 
				{
					if(flag_crc==false)
					{
						CRC16_Rx_ASCII(usart_rx_04[loop]);
					}
					else
					{
						if((usart_rx_04[loop]>='0')&&(usart_rx_04[loop]<='9')){
							crc<<=4;
							crc+= (usart_rx_04[loop]-'0');
						} else if((usart_rx_04[loop]>='A')&&(usart_rx_04[loop]<='F')){
							crc<<=4;
							crc+= ((usart_rx_04[loop]-'A')+10);
						} else if((usart_rx_04[loop]>='a')&&(usart_rx_04[loop]<='f')){
							crc<<=4;
							crc+= ((usart_rx_04[loop]-'a')+10);
						}

					}
				}
				else if(usart_rx_04[loop] == '*') 
				{
					flag_crc = true;
				}
				else if(usart_rx_04[loop] == ETX) 
				{
					flag_crc = false;

#if defined(DEBUG_UART_4_RX)

					if(crc == RXD_CRC)
					{
						printf("\r\nSBMS CRC OK = %04x / %04x", crc, RXD_CRC);
					}
					else
					{
						printf("\r\nSBMS CRC Error = %04x / %04x", crc, RXD_CRC);
					}
#endif 
					
				}
			}

			flag_rx_04 = 0;
		}
		
	}
}




