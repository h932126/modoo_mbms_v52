/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */
#ifndef ATMEL_START_PINS_H_INCLUDED
#define ATMEL_START_PINS_H_INCLUDED

#include <hal_gpio.h>

// SAMV71 has 4 pin functions

#define GPIO_PIN_FUNCTION_A 0
#define GPIO_PIN_FUNCTION_B 1
#define GPIO_PIN_FUNCTION_C 2
#define GPIO_PIN_FUNCTION_D 3

#define RS485_TX1_EN GPIO(GPIO_PORTA, 3)
#define PA4 GPIO(GPIO_PORTA, 4)
#define PA5 GPIO(GPIO_PORTA, 5)
#define PA9 GPIO(GPIO_PORTA, 9)
#define PA10 GPIO(GPIO_PORTA, 10)


#define RS485_TX3_EN GPIO(GPIO_PORTD, 31)
#define PD28 GPIO(GPIO_PORTD, 28)
#define PD30 GPIO(GPIO_PORTD, 30)


#define RS485_TX4_EN GPIO(GPIO_PORTD, 24)
#define PD18 GPIO(GPIO_PORTD, 18)
#define PD19 GPIO(GPIO_PORTD, 19)


#define PD25 GPIO(GPIO_PORTD, 25)
#define PD26 GPIO(GPIO_PORTD, 26)


#endif // ATMEL_START_PINS_H_INCLUDED
