#include <atmel_start.h>
#include "Update_List.h"
#include "rs485_async.h"

void Display_Device_Information(void)
{
	printf("\r\n");
	printf("//============================================\r\n");
	printf("// * RS485 Test Ver %d.%d%d\r\n", Device_Fw_version[0], Device_Fw_version[1], Device_Fw_version[2]);
	printf("//   - DEVICE : %s\r\n", DEVICE_NAME);	
	printf("//   - Compiled: %s / %s\r\n", __DATE__, __TIME__);	
	printf("//============================================\r\n");
}


unsigned char buff_02[] = "RS232-02-TX TEST ";

void rs232_tx_test_02(void);

void rs232_tx_test_02(void)
{
	rs232_tx_02( buff_02, 16 );
}




int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();

	rs485_init();
	
	rs232_init();

	// Display Device Inforamtion
	Display_Device_Information();

	
	/* Replace with your application code */
	while (1) {

		//====================================
		// ATS RS485 Tx Test / Address 0x1
		//====================================
#if 0
		send_ATS_UPS(0x01, 0x04, 40000, 10); 	// ATS Address 0x01
		delay_ms(1000);		
		rs485_get_rx_ATS_UPS( );
#endif

		//====================================
		// UPS RS485 Tx Test / Address 0x2		
		//====================================
#if 0		
		send_ATS_UPS(0x02, 0x04, 40000, 10); 	
		delay_ms(1000);		
		rs485_get_rx_ATS_UPS( );
#endif

		//====================================
		// VBOX RS485 Tx Test / Address 0x04 ~ 0x07
		//====================================
#if 0			
		send_VBOX(0x04, 0x04, 40000, 10); 		
		delay_ms(1000);			
		rs485_get_rx_VBOX( );		
#endif

		//====================================
		// LMS Test
		//====================================
#if 0		
		// Test Send LMS (RS232 - UART2)
		rs232_tx_test_02();
		delay_ms(500);	

		// Get LMS RS232 Data
		rs232_get_rx_LMS();
#endif
		//====================================
		// Sub_BMS Test
		//====================================
#if 1		
		send_SBMS(0x01, 0x10, 0x00);		
		delay_ms(500);
		rs485_get_rx_SBMS();	// get UART4 (SBMS)				
#endif
		
	}
}
