//=======================================================
// RS485 Test
//=======================================================
#define DEVICE_FW_LEN	3
#define DEVICE_NAME "Main BMS - V71"

static unsigned char Device_Fw_version[DEVICE_FW_LEN] = {0,5,2};

#define DEBUG_UART_1_TX		1		// VBOX
#define DEBUG_UART_1_RX		1		// VBOX

#define DEBUG_UART_2_TX		1		// LMS
#define DEBUG_UART_2_RX		1		// LMS

#define DEBUG_UART_3_TX		1		// ATS Device & UPS Device
#define DEBUG_UART_3_RX		1		// ATS Device & UPS Device

#define DEBUG_UART_4_TX		1		// SBMS
#define DEBUG_UART_4_RX		1		// SBMS

/*
//===========================================================
// Ver 0.5.2 / 2020.05.16
//===========================================================
1. LMS 통신확인 (RS232 USB 이용)
  1)Code 추가.
    - void rs232_get_rx_LMS( )
	- void rs232_tx_test_02(void)
	  >> 나중에 LMS와 연동 코드 추가 필요.

2. SBMS 통신확인
  - Tx : 7b,30,31,2c,31,30,2c,30,30,2c,30,30,2c,30,30,2a,44,41,31,30,7d  
  - Rx : 7b,30,31,2c,31,31,2c,38,30,2c,30,30,43,44,2c,39,46,35,38,2c,39,46,31,46,2c,39,46,33,32,2c,39,46,31,44,2c,39,46,31,39,2c,39,46,31,41,2c,39,46,31,45,2c,39,46,31,43,2c,39,46,31,45,2c,39,46,32,43,2c,39,46,32,45,2c,39,46,32,39,2a,41,46,36,45,7d,0d,0a,   

  1)Code 추가.
    void CRC16_Tx_ASCII(unsigned char dat)
	void ByteToChar2(uint8_t dt, uint8_t *buff, uint8_t p_index , uint8_t p_c , uint8_t p_crc)

3. Test Code 삭제
  
//===========================================================
// Ver 0.5.1 / 2020.05.15
//===========================================================
1. Code 정리.
	void send_VBOX(uint8_t p_addr, uint8_t p_func, uint16_t p_s_addr, uint16_t p_len);		// UART1(485)/115200bps/RTU : VBOX
	void send_ATS_UPS(uint8_t p_addr, uint8_t p_func, uint16_t p_s_addr, uint16_t p_len);	// UART3(485)/115200bps/RTU : ATS Device & UPS Device
	void send_SBMS(uint8_t p_addr, uint8_t p_func, uint16_t p_cmd);							// UART4(485)/9600bps/ASCII : Sub BMS

	void rs485_get_rx_VBOX( );				// UART1(485)/115200bps/RTU : VBOX
	void rs485_get_rx_ATS_UPS( );			// UART3(485)/115200bps/RTU : ATS Device & UPS Device
	void rs485_get_rx_SBMS( );				// UART4(485)/9600bps/ASCII : Sub BMS

2. VBOX 정합
  - Data 통신 확인 : 프로토콜은 ATS & UPS와 동일, 통신라인만 별도임. 

//===========================================================
// Ver 0.5.0 / 2020.05.13
//===========================================================
1. void Display_Device_Information(void) 함수 추가.
   - Device Name, Compile 시간정보, FW Version Debug 출력
	
2. UART Rx Callback 함수 수정.
   - UART Rx Callback에서는 Flag Set만 하고, 아래 함수에서 Data 수신.
     UART Rx Callback에서 Rx Pharsing Code를 넣으면 안됨.
   - void rs485_get_rx_01( )
   - void rs485_get_rx_03( )
   - void rs485_get_rx_04( )
   
4. UART Tx, Rx Buffer Size 조정 
	#define RX_DATA_MAX_SIZE 512	// 기존 16byte이었음. 너무 작음.
	#define TX_DATA_MAX_SIZE 16
	
5. 아래 ATS 장비와 통시 시 해당 보드에서 LED 반응이 있으나, Data 수신 안되는 현상.
	[문제 #1]
	  아래 ATS 장비와 통시 시 해당 보드에서 LED 반응이 있으나, Data 수신 확인 안됨.
	  # ATS 장비 (전력을 선택 - 제어) : Address 0x01
	  # UPS 장비 (전력 공급 장치) : Address 0x02
  
	[원인 및 체크사항]  
    
		void rs485_send(uint8_t p_port, uint8_t p_addr, uint8_t p_func, uint16_t p_s_addr, uint16_t p_len)
		{
			....	
			//==========================================================================
			// 확인필요
			// CRC 2byte가 Little Endian으로 처리해야 ATS Board 및 UPS Board와 통신 됨.
			//==========================================================================
			usart_tx_buffer[Tx_cnt++] = (unsigned char)((crc16>>0) & 0x00ff);
			usart_tx_buffer[Tx_cnt++] = (unsigned char)((crc16>>8) & 0x00ff);
		
			//==========================================================================
			// Error 발생.	
			// CRC 2byte가 Big Endian으로 처리 시  ATS Board 및 UPS Board와 통신 안됨.
			//==========================================================================
			usart_tx_buffer[Tx_cnt++] = (unsigned char)((crc16>>8) & 0x00ff);
			usart_tx_buffer[Tx_cnt++] = (unsigned char)((crc16>>0) & 0x00ff);
			....
		}

4. ATS Board 및 UPS Board와 통신 시 Data가 완벽하게 수신되지 않는 경우 발생.
  - ATS Board 및 UPS Board에서 전송되는 초기 Data Loss 현상 발생.
  
  [원인]
  - Uart Rx Callback 함수에서 "ADM2682E"의 DE(Device Enable Input)제어 시 Delay_ms(5)가 영향을 줌.      
	
  [수정]    
  - 최대한 빠르게 "ADM2682E"의 DE(Device Enable Input)를 low로 설정해야 함.
    "ADM2682E"의 DE(Device Enable Input)이 High일 경우 Rx 수신 안됨.
    static void rs485_tx_cb_03(const struct usart_async_descriptor *const io_descr)
	{
		// Transfer completed 
		//delay_ms(5);
		gpio_set_pin_level(RS485_TX3_EN, false);
	}
   


	
	
*/
