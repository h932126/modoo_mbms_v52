#ifndef __RS485_ASYNC_H__
#define __RS485_ASYNC_H__

#define RS485_VBOX	  0x01		// UART1(485)/115200bps/RTU : ATS Device & UPS Device
#define RS485_P02 	  0x02
#define RS485_ATS  	  0x03		// UART3(485)/115200bps/RTU : VBOX
#define RS485_P04  	  0x04		// UART4(485)/9600bps/ASCII : Sub BMS


void rs485_init(void);
void rs485_tx( uint8_t *buff, uint16_t length );
void rs232_tx_02( uint8_t *buff, uint16_t length );
void rs485_tx_03( uint8_t *buff, uint16_t length );
void rs485_tx_04( uint8_t *buff, uint16_t length );

unsigned short CRC16(const unsigned char *nData, unsigned char wLength);
void CRC16_Tx_ASCII(unsigned char dat);

void send_VBOX(uint8_t p_addr, uint8_t p_func, uint16_t p_s_addr, uint16_t p_len);		// UART1(485)/115200bps/RTU : VBOX
void send_ATS_UPS(uint8_t p_addr, uint8_t p_func, uint16_t p_s_addr, uint16_t p_len);	// UART3(485)/115200bps/RTU : ATS Device & UPS Device
void send_SBMS(uint8_t p_addr, uint8_t p_func, uint16_t p_cmd);						// UART4(485)/9600bps/ASCII : Sub BMS

void rs485_get_rx_VBOX( );				// UART1(485)/115200bps/RTU : VBOX
void rs232_get_rx_LMS( );	  				// UART2(232)/115200bps/RTU : LMS
void rs485_get_rx_ATS_UPS( );			// UART3(485)/115200bps/RTU : ATS Device & UPS Device
void rs485_get_rx_SBMS( );				// UART4(485)/9600bps/ASCII : Sub BMS

#endif // __RS485_UART1_ASYNC_H__